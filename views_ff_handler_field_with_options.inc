<?php

class views_ff_handler_field_with_options extends views_ff_handler_field {

  var $allowed_values;
  function option_definition() {
    $options = parent::option_definition();
    $options['field_type'] = array('default' => 'select');
    $options['allowed_values'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['field_type'] = array(
      '#title' => t('Field type'),
      '#type' => 'radios',
      '#default_value' => $this->options['field_type'],
      '#description' => t('Select what kind of field use to select options.'),
      '#options' => array(
        'select' => t('Select'),
        'checkboxes' => t('Checkboxes'),
        'radios' => t('Radios'),
      ),
    );

    $form['allowed_values'] = array (
      '#title' => t('Allowed values'),
      '#type' => 'textarea',
      '#default_value' => $this->options['allowed_values'],
      '#description' => t('The possible values this field can contain. Enter one value per line, in the format key|label. The label is optional, and the key will be used as the label if no label is specified.'),
    );
  }

  function form_properties() {
    $properties = array();
    // explicity add this properties in live preview.
    // without #proccess here, radios and checkboxes are not rendered
    if (!empty($this->view->live_preview)) {
      $properties['#parents'] = array();
      if ($this->form_type == 'radios') {
        $properties['#process'] = 'expand_radios';
      }
      elseif ($this->form_type == 'checkboxes') {
        $properties['#process'] = 'expand_checkboxes';
      }
    }
    $properties['#options'] = $this->allowed_values();
    return $properties;
  }

  function allowed_values() {
    $list = explode("\n", $this->options['allowed_values']);
    list($key, $value) = explode('|', $opt);

    $allowed_values = array();
    $list = explode("\n", $this->options['allowed_values']);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');
    foreach ($list as $opt) {
      // Sanitize the user input with a permissive filter.
      $opt = content_filter_xss($opt);
      if (strpos($opt, '|') !== FALSE) {
        list($key, $value) = explode('|', $opt);
        $allowed_values[$key] = (isset($value) && $value !=='') ? $value : $key;
      }
      else {
        $allowed_values[$opt] = $opt;
      }
    }
    return $allowed_values;
  }

  function pre_render() {
    parent::pre_render();
    $options = $this->form_properties();
    $this->allowed_values = $this->allowed_values();
    $this->form_type = isset($this->options['field_type']) ? $this->options['field_type'] : 'select';
  }

  function render_no_edit($field_id, $from) {
    if ($from == 'node') {
      $value = $this->view->display_handler->node->{$this->view->name . '_' .$this->view->current_display}[$this->options['machine_name']][$field_id];
      $default_value = $this->default_value;

      if (intval($value) != intval($default_value)) {
        return '<span class = "modified">' . check_plain($this->allowed_values[$value]) . '</span>';
      }
      else {
        return check_plain($this->allowed_values[$value]);
      }
    }
    elseif ($from == 'form') {
      return $this->default_value;
    }
  }

  function get_default_value($values) {
    $this->default_value = array(parent::get_default_value($values));
    return $this->default_value;
  }

}
