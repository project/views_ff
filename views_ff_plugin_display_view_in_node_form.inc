<?php

/**
 * The plugin that handles node-attached views.
 */
class views_ff_plugin_display_view_in_node_form extends views_ff_plugin_display_view_in_form {
  var $node;

  function set_node($node) {
    $this->node = $node;
  }

  function get_node() {
    return $this->node;
  }

  function option_definition () {
    $options = parent::option_definition();

    $options['types'] = array('default' => array());
    $options['display_only'] = array('default' => array());
    $options['argument_mode'] = array('default' => 'nid');
    $options['default_argument'] = array('default' => '');
    $options['show_title'] = 0;

    return $options;
  }

  /**
   * Provide the summary for page options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    parent::options_summary($categories, $options);

    $categories['view_in_form'] = array(
      'title' => t('View in node form settings'),
    );

    $node_types = $this->get_option('types');
    if (empty($node_types)) {
      $types = t('- none selected -');
    }
    else {
      $types = implode(', ', $node_types);
    }

    $options['types'] = array(
      'category' => 'view_in_form',
      'title' => t('Node types'),
      'value' => $types
    );

    $display_only = $this->get_option('display_only');
    $options['on_preview'] = array(
      'category' => 'view_in_form',
      'title' => t('On node preview'),
      'value' => count($display_only) ? t('Display rows with modified values') : t('Display all rows'),
    );

    unset($options['weight']);
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'types':
        $form['#title'] .= t('Node types');
        $form['types'] = array(
          '#type' => 'checkboxes',
          '#multiple' => TRUE,
          '#required' => TRUE,
          '#title' => t('Embed this display in the following node types'),
          '#options' => node_get_types('names'),
          '#default_value' => $this->get_option('types'),
        );

        break;
      case 'on_preview':
        $form['#title'] .= t('On node preview');

        $form['description'] = array(
          '#prefix' => '<div class="description form-item">',
          '#suffix' => '</div>',
          '#value' =>  t('Here you can define a script to filter the results to display when node is previewed.
            You probably may want to display only rows where default value are different than actually selected.'),
        );

        $handlers = $this->display->handler->get_handlers('field');
        if (count($handlers)) {
          foreach ($handlers as $field) {
            if (!empty($field->definition['form field'])) {
              $options[$field->field] = t('@label field has changed.', array('@label' => $field->options['label']));
            }
          }
        }

        $form['display_only'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Display row only if'),
          '#options' => $options,
          '#default_value' => $this->get_option('display_only'),
          '#description' => t('When user click on preview you may want to display only the rows with modified values, leave this empty to display all the rows'),
        );
        $this->view->init_style();
        if (empty($this->view->style_plugin->definition['views form fields'])) {
          $form['display_only']['#disabled'] = TRUE;
          drupal_set_message(t('You need to enable the "Rows with modified values" style plugin to use the "Display row only if" feature.'), 'warning');
        }
        //@TODO
      case 'weight':
        $form['#title'] .= t('Form weight');
        $form['weight']['#type'] = 'value';
        break;
    }
  }

  function options_submit($form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'types':
        $this->set_option('types', array_filter($form_state['values']['types']));
        break;
      case 'on_preview':
        $this->set_option('display_only', array_filter($form_state['values']['display_only']));
        break;
    }
  }

}
