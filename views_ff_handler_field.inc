<?php

class views_ff_handler_field extends views_handler_field {
  var $can_edit;
  var $form_type;
  var $default_value;

  function init(&$view, $options) {
    parent::init($view, $options);

    // Flag to indicate wheter the actual user can edit the field
    $this->can_edit = FALSE;
  }

  function option_definition() {
    $options['label'] = array('default' => $this->definition['title'], 'translatable' => TRUE);
    $options['machine_name'] = array('default' => '', 'translatable' => FALSE);
    $options['required'] = array('default' => FALSE, 'bool' => TRUE);
    $options['default'] = array('default' => 'field');
    $options['source'] = array('default' => '');
    $options['value_if_empty'] = array('default' => FALSE, 'bool' => TRUE);
    $options['custom'] = array('default' => '');
    $options['role'] = array('default' => array());
    $options['restricted_field'] = array('default' => 'disabled');
    return $options;
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function options_form(&$form, &$form_state) {
    // Process parent for to get available tokens
    $parent_form = $parent_form_state = array();
    parent::options_form($parent_form, $parent_form_state);

    // Reuse label from parent form
    $form['label'] = $parent_form['label'];

    $form['machine_name'] = array(
      '#title' => t('Machine name'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->options['machine_name'],
      '#description' => t('Define a machine name for this field.'),
    );

    $form['required'] = array(
      '#title' => t('Required'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['required'],
      '#description' => t('If checked users must complete this field.'),
    );

    $options = array('' => t('- Select one -'));
    $fields = $this->view->display_handler->get_handlers('field');
    if (is_array($fields)) {
      foreach ($fields as $field_id => $field) {
        if ($field->definition['group'] != $this->definition['group']) {
          $options[$field_id] = check_plain($field->label());
        }
      }
    }

    $form['default'] = array(
      '#title' => t('Default value'),
      '#type' => 'radios',
      '#default_value' => $this->options['default'],
      '#options' => array(
        'field' => t('Take value from field'),
        'custom'=> t('Custom value'),
      ),
      '#description' => t('The default value for this form field.'),
    );

    $form['source'] = array(
      '#title' => t('Source field'),
      '#type' => 'select',
      '#default_value' => $this->options['source'],
      '#options' => $options,
      '#process' => array('views_process_dependency'),
      '#dependency' => array('radio:options[default]' => array('field')),
      '#description' => t('Select which field use to load the default value for this form element.'),
    );

    $form['value_if_empty'] = array(
      '#type' => 'textfield',
      '#title' => t('Default value for empty field'),
      '#default_value' => $this->options['value_if_empty'],
      '#process' => array('views_process_dependency'),
      '#dependency' => array('radio:options[default]' => array('field')),
      '#description' => t('If the field does not have a value, you can define one here.'),
    );

    $form['custom'] = array(
      '#type' => 'textfield',
      '#title' => t('Default value'),
      '#default_value' => $this->options['custom'],
      '#process' => array('views_process_dependency'),
      '#dependency' => array('radio:options[default]' => array('custom')),
    );

    $roles = views_ui_get_roles();
    $form['role'] =  array(
      '#type' => 'checkboxes',
      '#title' => t('Edit permissions'),
      '#default_value' => $this->options['role'],
      '#options' => $roles,
      '#description' => t('Only the checked roles will be able to edit this field. Leave empty for no access control.'),
    );

    $form['restricted_field'] =  array(
      '#type' => 'radios',
      '#title' => t('If user cannot edit this field'),
      '#default_value' => $this->options['restricted_field'],
      '#options' => array(
        'value' => t('Pass as a hidden field value.'),
        'exclude' => t('Exclude field.'),
        'source' => t('Display value from source field.'),
        'source_value' => t('Display value from source field and as a hidden field.'),
        'disabled' => t('Display field as a disabled form field.'),
      ),
      '#description' => t('Select wich action perform if user cannot edit this field.'),
    );
  }

  /**
   * Define basic settings for this form element, other handlers may
   * override this function.
   */
  function get_form_element() {
    $form = array(
      '#type' => 'item',
      '#title' => $this->get_label(),
    );
    return $form;
  }

  function can_edit() {
    $roles = $this->options['role'];
    if (count($roles)) {
      global $user;
      if ($user->uid == 1) {
        return TRUE;
      }
      else {
        foreach ($user->roles as $rid => $role) {
          if (in_array($rid, $roles)) {
            return TRUE;
            break;
          }
        }
      }
    }
    else {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Pre render is used to determine user access to this field.
   */
  function pre_render() {
    $this->can_edit = $this->can_edit();
  }

  /**
   * Override render_text from views_handler_field.
   */
  function render_text($alter) {
    if (empty($this->can_edit) && $this->options['restricted_field'] == 'exclude') {
      return '';
    }

    $machine_name = $this->options['machine_name'];
    $display_identifier = $this->view->name . '_' .$this->view->current_display;
    $value = trim($this->last_render);

    // Use replace patters to build a unique id for this field
    $fake_alter['alter_text'] = TRUE;
    $fake_alter['text'] = $this->options['item_id'];
    $tokens = $this->get_render_tokens($fake_alter);
    $this->item_id = $item_id = $this->render_altered($fake_alter, $tokens);

    $form_options = array(
      '#type' => $this->form_type,
      '#default_value' => $this->default_value,
    );
    if ($this->can_edit == FALSE) {
      switch ($this->options['restricted_field']) {
        case 'exclude':
          return '';
        case 'value':
          $form_options['type'] = 'value';
          break;
        case 'source':
          return check_plain($form_options['#default_value']);
          break;
        case 'source_value':
          $source_value = TRUE;
          break;
        case 'disabled':
          $form_options['#disabled'] = TRUE;
          break;
      }
    }

    // Ensure we are using a display that alter a form
    if (!empty($this->view->display_handler->accept_form_fields) && !$this->view->live_preview) {
      // If the node is present, this is the preview of the node
      if (!empty($this->view->display_handler->node)) {
        return $this->render_no_edit('node');
      }

      // A form alter is in execution, add the fields as form api elements
      $form = &$this->view->display_handler->parent_form;
      if ($this->view->display_handler->get_performing_form_alter()) {

        $form_state = &$this->view->display_handler->parent_form_state;
        if (isset($form_state['values'][$display_identifier][$this->view->row_index][$machine_name])) {
          $form_options['#default_value'] = $form_state['values'][$display_identifier][$this->view->row_index][$machine_name];
        }

        if (!isset($form[$display_identifier][$this->view->row_index])) {
          // Put fields into an array to group the values when form is submitted
          $form[$display_identifier][$this->view->row_index] = array();
        }

        // Add the field as a form element in the parent form
        $form[$display_identifier][$this->view->row_index][$machine_name] = $form_options;

        // Allow to children class to add new properties to the form
        $form[$display_identifier][$this->view->row_index][$machine_name] += $this->form_properties();
      }
      else {
        if ($type == 'value' && !empty($source_value)) {
          return $this->render_no_edit('form') . drupal_render($form[$this->view->row_index][$machine_name]);
        }
        else {
          return drupal_render($form[$this->view->row_index][$machine_name]);
        }
      }
    }
    else {
      // Only render fields in live preview.
      // If display_handler->accept_form_fields == FALSE, don't render
      // forms field, this force to use Form API instead of $_POST.
      if (!empty($this->view->live_preview)) {
        return $this->render_in_live_preview($form_options['#default_value']);
      }
    }
  }

  /**
   * Due live preview doesn't provide a form, render widgets using a fake form.
   */
  function render_in_live_preview($default_value) {
    $form = array(
      '#type' => $this->form_type,
      '#value' => $default_value,
    );
    if (!empty($disabled)) {
      $form['#attributes'] = array('disabled' => 'disabled');
    }
    $form += $this->form_properties();

    if (!empty($form['#process'])) {
      $function = $form['#process'];
      $form = $function($form);
    }

    return drupal_render($form);
  }

  /**
   * This function render the selected value of a form like a field,
   * useful when previewing nodes submissions.
   */
  function render_no_edit($from) {
    if ($from == 'node') {
      $value = $this->view->display_handler->node->{$this->view->name . '_' .$this->view->current_display}[$this->view->row_index][$this->options['machine_name']];
      $default_value = $this->default_value;
      if ($value != $default_value) {
        return '<span class = "modified">' . check_plain($value) . '</span>';
      }
      else {
        return check_plain($value);
      }
    }
    elseif ($from == 'form') {
      return $this->default_value;
    }
  }

  function render($values) {
    // Get default value on render,
    // here we can know what values are defined for other fields.
    $this->get_default_value($values);
    parent::render($values);
  }

  /**
   * Obtains the default value of this field based on field settings.
   */
  function get_default_value($values) {
    $default_value = '';
    if ($this->options['default'] == 'field' && !empty($this->options['source'])) {
      $field_alias = $this->view->display_handler->handlers['field'][$this->options['source']]->field_alias;
      $default_value = $values->$field_alias;
      if (empty($default_value) && !empty($this->options['value_if_empty'])) {
        $default_value = $this->options['value_if_empty'];
      }
    }
    elseif ($this->options['default'] == 'custom') {
      $default_value = $this->options['custom'];
    }
    $this->default_value = $default_value;
  }

  function form_properties() {
    return array();
  }

  /**
   * Returns TRUE if the field has changed from its original default value.
   */
  function has_changed($value, $row_id) {
    $this->get_default_value($this->view->result[$row_id]);
    return $value != $this->default_value;
  }
}
