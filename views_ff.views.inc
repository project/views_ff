<?php

/**
 * Implementation of hook_views_plugins().
 */
function views_ff_views_plugins() {
  return array(
    'display' => array(
      'view_in_form' => array(
        'title' => t('View in generic form'),
        'help' => t('Include this views as part of a form.'),
        'handler' => 'views_ff_plugin_display_view_in_form',
        'theme' => 'views_ff_plugin_display_view_in_form',
        'views in form' => TRUE,
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'use more' => TRUE,
        'accept attachments' => TRUE,
        'admin' => t('View in node form'),
        'help topic' => 'display-view-in-form',
      ),
      'view_in_node_form' => array(
        'title' => t('View in node form'),
        'help' => t('Include this views as part of the node form.'),
        'handler' => 'views_ff_plugin_display_view_in_node_form',
        'theme' => 'views_ff_plugin_display_view_in_node_form',
        'views in node form' => TRUE,
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'use more' => TRUE,
        'parent' => 'view_in_form',
        'accept attachments' => TRUE,
        'admin' => t('View in node form'),
        'help topic' => 'display-view-in-node-form',
      ),
    ),
    'style' => array(
      'views_ff_modified_rows' => array(
        'title' => t('Rows with modified values'),
        'help' => t('Display table rows only if the default value has changed in a form field.'),
        'handler' => 'views_ff_plugin_style_table_modified_rows',
        'theme' => 'views_view_table',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => TRUE,
        'type' => 'normal',
        'views form fields' => TRUE,
        'help topic' => 'style-unformatted',
        'parent' => 'table'
      ),
    ),
  );
}

/**
 * Implementation of hook_views_handlers().
 */
function views_ff_views_handlers() {
  return array(
    'handlers' => array(
      'views_ff_handler_field' => array(
        'form field' => TRUE,
        'parent' => 'views_handler_field',
        'file' => 'views_ff_handler_field.inc',
      ),
      'views_ff_handler_field_textfield' => array(
        'parent' => 'views_ff_handler_field',
        'file' => 'views_ff_handler_field_textfield.inc',
      ),
      'views_ff_handler_field_textarea' => array(
        'parent' => 'views_ff_handler_field',
        'file' => 'views_ff_handler_field_textarea.inc',
      ),
      'views_ff_handler_field_value' => array(
        'parent' => 'views_ff_handler_field',
        'file' => 'views_ff_handler_field_value.inc',
      ),
      'views_ff_handler_field_with_options' => array(
        'parent' => 'views_ff_handler_field',
        'file' => 'views_ff_handler_field_with_options.inc',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data()
 */
function views_ff_views_data() {

  $data['ff']['table']['group']  = t('Form Element');
  $data['ff']['table']['join'] = array(
    '#global' => array(),
  );

  $data['ff']['textfield'] = array(
    'title' => t('Textfield'),
    'help' => t('A textfield form element.'), 
    'field' => array(
      'form field' => TRUE,
      'handler' => 'views_ff_handler_field_textfield',
      'click sortable' => FALSE,
    ),
  );

  $data['ff']['textarea'] = array(
    'title' => t('Textarea'),
    'help' => t('A textarea form element.'), 
    'field' => array(
      'form field' => TRUE,
      'handler' => 'views_ff_handler_field_textarea',
      'click sortable' => FALSE,
    ),
  );

  $data['ff']['value'] = array(
    'title' => t('Value'),
    'help' => t('A value form element. Usefull to pass hidden values.'), 
    'field' => array(
      'form field' => TRUE,
      'handler' => 'views_ff_handler_field_value',
      'click sortable' => FALSE,
    ),
  );

  $data['ff']['with_options'] = array(
    'title' => t('With Options'),
    'help' => t('A form element with multiple options (select, radios, checkboxes).'), 
    'field' => array(
      'form field' => TRUE,
      'handler' => 'views_ff_handler_field_with_options',
      'click sortable' => FALSE,
    ),
  );
  return $data;
}
