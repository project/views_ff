<?php

class views_ff_handler_field_textarea extends views_ff_handler_field {

  function option_definition() {
    $options = parent::option_definition();
    $options['rows'] = array('default' => 5);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['rows'] = array(
      '#type' => 'textfield',
      '#title' => t('rows'),
      '#default_value' => $this->options['rows'],
    );
  }

  function form_properties() {
    return array (
      '#rows' => $this->options['rows'],
    );
  }
}
