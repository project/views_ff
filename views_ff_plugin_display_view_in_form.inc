<?php

/**
 * The plugin that handles form-attached views.
 */
class views_ff_plugin_display_view_in_form extends views_plugin_display {
  var $parent_form;
  var $parent_form_state;
  var $accept_form_fields;
  var $is_form_alter;

  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);

    $this->accept_form_fields = TRUE;
    $this->parent_form = array();
  }

  function set_parent_form($form) {
    $this->parent_form = $form;
    $this->parent_form_state = $form_state;
  }

  function set_parent_form_state($form_state) {
    $this->parent_form_state = $form_state;
  }

  function get_parent_form() {
    return $this->parent_form;
  }

  function set_performing_form_alter($state) {
    $this->is_form_alter = $state;
  }

  function get_performing_form_alter() {
    return $this->is_form_alter;
  }

  function option_definition () {
    $options = parent::option_definition();

    $options['types'] = array('default' => array());
    $options['show_title'] = 0;
    $options['weight'] = array('default' => 10);
    $options['form_ids'] = array('default' => array());
    return $options;
  }

  /**
   * Provide the summary for page options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    parent::options_summary($categories, $options);

    $categories['view_in_form'] = array(
      'title' => t('View in form settings'),
    );

    $weight = $this->get_option('weight');
    if (empty($weight)) {
      $weight = 10;
    }

    $options['show_title'] = array(
      'category' => 'view_in_form',
      'title' => t('Show title'),
      'value' => $this->get_option('show_title') ? t('Yes') : t('No'),
    );

    $options['weight'] = array(
      'category' => 'view_in_form',
      'title' => t('Weight'),
      'value' => $this->get_option('weight'),
    );

  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'show_title':
        $form['#title'] .= t('Show title');
        $form['show_title'] = array(
          '#type' => 'checkbox',
          '#title' => t('Show the title of the view above the attached view.'),
          '#default_value' => $this->get_option('show_title'),
        );
        break;
      case 'weight':
        $form['#title'] .= t('Form weight');
        $form['weight'] = array(
          '#type' => 'weight',
          '#title' => t('Weight'),
          '#description' => t('The heavier elements will sink and the lighter elements will be positioned nearer the top.'),
          '#delta' => 20,
          '#default_value' => $this->get_option('weight'),
        );
        break;
    }

  }

  function options_submit($form, &$form_state) {
    parent::options_submit($form, $form_state);

    switch ($form_state['section']) {
      case 'show_title':
        $this->set_option('show_title', $form_state['values']['show_title']);
        break;
      case 'weight':
        $this->set_option('show_title', $form_state['values']['weight']);
        break;
    }
  }

  /**
   * Node content views use exposed widgets only if AJAX is set.
   */
  function uses_exposed() {
    if ($this->use_ajax()) {
      return parent::uses_exposed();
    }
    return FALSE;
  }

  /**
   * The display block handler returns the structure necessary for a block.
   */
  function execute() {
    $data = $this->view->render();
    if (!empty($this->view->result) || $this->get_option('empty') || !empty($this->view->style_plugin->definition['even empty'])) {
      return $data;
    }
  }

}
