<?php

class views_ff_handler_field_value extends views_ff_handler_field {

  function init(&$view, $options) {
    parent::init($view, $options);
    
    $this->form_type = 'value';
  }

  function option_definition() {
    $options['label'] = array('default' => $this->definition['title'], 'translatable' => TRUE);
    $options['item_id'] = array('default' => strtolower($this->definition['title']) . '-[nid]', 'translatable' => FALSE);
    $options['machine_name'] = array('default' => '', 'translatable' => FALSE);
    $options['default'] = array('default' => 'field');
    $options['source'] = array('default' => '');
    $options['value_if_empty'] = array('default' => FALSE, 'bool' => TRUE);
    $options['custom'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    unset($form['required']);
    unset($form['role']);
    unset($form['restricted_field']);
  }

  function form_properties() {
    return array(
      '#value' => $this->default_value,
    );
  }
}
