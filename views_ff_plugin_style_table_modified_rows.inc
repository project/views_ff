<?php


class views_ff_plugin_style_table_modified_rows extends views_plugin_style_table {

  /**
   * Render the display in this style.
   */
  function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      vpr('views_plugin_style_default: Missing row plugin');
      return;
    }

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each group separately and concatenate.  Plugins may override this
    // method if they wish some other way of handling grouping.
    $output = '';
    $x = 0;
    foreach ($sets as $title => $records) {
      if ($this->uses_row_plugin()) {
        $rows = array();
        foreach ($records as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[] = $this->row_plugin->render($row);
        }
      }
      else {
        $rows = $records;
      }
      $form_values = $this->display->handler->node->{$this->view->name . '_' . $this->display->id};
      $check_modified_fields = $this->display->handler->options['display_only'];
      $form_fields = array();
      foreach ($check_modified_fields as $field) {
        $machine = $this->display->handler->handlers['field'][$field]->options['machine_name'];
        $form_fields[$machine] = $field;
      }
      
      if (is_array($form_values) && count($form_fields)) {
        foreach ($form_values as $row_id => $values) {
          $display_row = FALSE;
          foreach ($values as $machine => $value) {
            $display_row |= $this->display->handler->handlers['field'][$form_fields[$machine]]->has_changed($form_values[$row_id][$machine], $row_id);
          }
          if (!$display_row) {
            unset($rows[$row_id]);
          }
        }
      }
      $output .= theme($this->theme_functions(), $this->view, $this->options, $rows, $title);
    }
    unset($this->view->row_index);
    return $output;
  }
}
