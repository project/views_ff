<?php

class views_ff_handler_field_textfield extends views_ff_handler_field {

  function init(&$view, $options) {
    parent::init($view, $options);
    
    $this->form_type = "textfield";
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['type'] = array('default' => 'text');
    $options['size'] = array('default' => 20);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['type'] = array(
      '#title' => t('Field type'),
      '#type' => 'radios',
      '#default_value' => $this->options['type'],
      '#description' => t('Select what kind of values will accept this field'),
      '#options' => array(
        'text' => t('Text'),
        'int' => t('Integer'),
        'float' => t('Float'),
      ),
    );

    $form['size'] = array (
      '#title' => t('Size'),
      '#type' => 'textfield',
      '#default_value' => $this->options['size'],
      '#description' => t('Size of the textfield.'),
    );
  }

  function form_properties() {
    return array (
      '#size' => $this->options['size'],
    );
  }

}
